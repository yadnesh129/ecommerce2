-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.26 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for ecommerce_db
CREATE DATABASE IF NOT EXISTS `ecommerce_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ecommerce_db`;

-- Dumping structure for table ecommerce_db.hibernate_sequence
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table ecommerce_db.hibernate_sequence: ~0 rows (approximately)
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES
	(74);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;

-- Dumping structure for table ecommerce_db.location
CREATE TABLE IF NOT EXISTS `location` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table ecommerce_db.location: ~4 rows (approximately)
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` (`id`, `address`, `city`, `state`, `country`) VALUES
	(1, 'Aundh', 'Pune', 'Maharashtra', 'India'),
	(2, 'Dadar', 'Mumbai', 'Maharashtra', 'India'),
	(3, 'Banglore', 'Banglore', 'Karantaka', 'India');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;

-- Dumping structure for table ecommerce_db.non_perishable_product
CREATE TABLE IF NOT EXISTS `non_perishable_product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `service_location` bigint DEFAULT NULL,
  `warranty` bigint DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_non_perishable_items_location` (`service_location`),
  CONSTRAINT `FK_non_perishable_items_location` FOREIGN KEY (`service_location`) REFERENCES `location` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table ecommerce_db.non_perishable_product: ~2 rows (approximately)
/*!40000 ALTER TABLE `non_perishable_product` DISABLE KEYS */;
INSERT INTO `non_perishable_product` (`id`, `service_location`, `warranty`) VALUES
	(42, 1, 24),
	(71, 1, 36);
/*!40000 ALTER TABLE `non_perishable_product` ENABLE KEYS */;

-- Dumping structure for table ecommerce_db.perishable_product
CREATE TABLE IF NOT EXISTS `perishable_product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `best_before` bigint DEFAULT NULL,
  `ingredients` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `health_warning` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table ecommerce_db.perishable_product: ~1 rows (approximately)
/*!40000 ALTER TABLE `perishable_product` DISABLE KEYS */;
INSERT INTO `perishable_product` (`id`, `best_before`, `ingredients`, `health_warning`) VALUES
	(48, 0, 'Cheese', 'Fat');
/*!40000 ALTER TABLE `perishable_product` ENABLE KEYS */;

-- Dumping structure for table ecommerce_db.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` double NOT NULL,
  `seller` varchar(255) NOT NULL,
  `product_location` bigint NOT NULL,
  `product_type_perishable` bigint DEFAULT NULL,
  `product_type_non_persishable` bigint DEFAULT NULL,
  `search_str` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_location` (`product_location`),
  KEY `FK_product_perishable_product` (`product_type_perishable`),
  KEY `FK_product_non_perishable_items` (`product_type_non_persishable`),
  CONSTRAINT `FK_product_location` FOREIGN KEY (`product_location`) REFERENCES `location` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_product_non_perishable_items` FOREIGN KEY (`product_type_non_persishable`) REFERENCES `non_perishable_product` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_product_perishable_product` FOREIGN KEY (`product_type_perishable`) REFERENCES `perishable_product` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table ecommerce_db.product: ~3 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `name`, `description`, `price`, `seller`, `product_location`, `product_type_perishable`, `product_type_non_persishable`, `search_str`) VALUES
	(41, 'Mouse', 'Black', 1000, 'Amazon', 1, NULL, 42, NULL),
	(47, 'Cheese', 'Cheesy', 10000, 'Amul', 3, 48, NULL, NULL),
	(70, 'mobile', 'Black', 25000, 'Cloudtail', 3, NULL, 71, NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table ecommerce_db.shopping_cart
CREATE TABLE IF NOT EXISTS `shopping_cart` (
  `location_id` bigint NOT NULL,
  `id` bigint NOT NULL AUTO_INCREMENT,
  `product_id` bigint NOT NULL DEFAULT '0',
  `delivery_time` int DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_shopping_cart_location` (`location_id`),
  KEY `FK_shopping_cart_product` (`product_id`),
  CONSTRAINT `FK_shopping_cart_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_shopping_cart_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table ecommerce_db.shopping_cart: ~9 rows (approximately)
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` (`location_id`, `id`, `product_id`, `delivery_time`, `total_amount`) VALUES
	(1, 53, 47, NULL, NULL),
	(1, 56, 47, NULL, NULL),
	(3, 58, 41, NULL, NULL),
	(2, 62, 41, NULL, NULL),
	(1, 64, 41, NULL, NULL),
	(1, 65, 47, NULL, NULL),
	(3, 66, 41, NULL, NULL),
	(1, 72, 70, NULL, NULL),
	(1, 73, 70, NULL, NULL);
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;

-- Dumping structure for table ecommerce_db.shopping_cart_bill
CREATE TABLE IF NOT EXISTS `shopping_cart_bill` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `delivery_time` int NOT NULL,
  `total_amount` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table ecommerce_db.shopping_cart_bill: ~1 rows (approximately)
/*!40000 ALTER TABLE `shopping_cart_bill` DISABLE KEYS */;
INSERT INTO `shopping_cart_bill` (`id`, `delivery_time`, `total_amount`) VALUES
	(59, 3, 84000);
/*!40000 ALTER TABLE `shopping_cart_bill` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
