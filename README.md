# ecommerce2

Branch to clone is Master 
Postman collections are added in the main branch
Database file for creating the tables & data is added in the sql file called Ecommmerce_db in main branch

Endpoints are ->
root endpoint: http://localhost:8080
Angular endpoints are ->
    1>'product'
        description :redirect to the product page
        1.1>''
            description : product start page
        1.2>'new'
            description : create a new product page
        1.3>':id'
            description : product details page from where you can order a product
        1.4>':id/edit'
            description : edit product page
    2>'shopping-list'
        description : shopping list page which shows the shopping list along with the shopping bill (total amount && & delivery time)       
Springboot  Endpoints are->
    1>/product/search(POST)
        body -> {}
        description : search for all products
    2>/product/add(POST)
        body->product dto 
        description-> send product details in dto to add a product
    3>/product/delete(POST)
        body->{"id":""}
        description->send the product body with id to delete the product
    4>/checkout
        4.1>/shoppingCart/add(POST)
            body->{"id","location":"Pune"}
            description-> send the product dto with id and delivery location(only Pune,Mumbai and Banglore)
                        ->The user deliver location is taken from the  location set on the first product  added to the shopping list (it is basically hardcoded)
        4.2>/shoppingCart/get(GET)
            description->Get all the products in shopping list
        4.3>/shoppingCart/getBill
            description->Get bill details i.e. total amount and delivery time


    
